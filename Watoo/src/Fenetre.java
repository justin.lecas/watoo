import javax.swing.*;

public class Fenetre extends JFrame {
    private JFrame fenetre;

    public Fenetre(String title, int width, int height){
        this.fenetre = new JFrame();
        this.fenetre.setTitle(title);
        this.fenetre.setSize(width, height);
        this.fenetre.setLocationRelativeTo(null);
        this.fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.fenetre.setVisible(true);
    }

    public Fenetre(String title){
        this(title, 500, 500);
    }

    public Fenetre(int width, int height){
        this("Fenetre", width, height);
    }
}
