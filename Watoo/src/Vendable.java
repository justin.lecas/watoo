public interface Vendable {

    public String getNom();

    public String getType();

    /*
    * L'état d'un article est un nombre de 0 à 100. 0 étant à jeter à la poubelle, et 100 neuf.
    */
    public double getEtat();

    public String getInfos();

    /*
    * Cette méthode améliore l'état de 10 points. Ne peut pas dépasser 100.
    */
    public void reparer();

    public String getImage();
}
