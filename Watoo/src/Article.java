public abstract class Article implements Vendable {
    private String nom;
    private double etat;
    protected String image;

    public Article(String nom, double etat, String image){
        this.nom = nom;
        this.etat = etat;
        this.image = image;
    }

    public String getNom() {
        return this.nom;
    }

    public double getEtat(){
        return this.etat;
    }

    public String getImage() { return this.image; }
}
